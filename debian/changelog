libtut (0.0.20070706-4) unstable; urgency=medium

  * QA upload.

  * Added d/gbp.conf to describe branch layout.
  * Updated vcs in d/control to Salsa.
  * Updated d/gbp.conf to enforce the use of pristine-tar.
  * Updated Standards-Version from 4.5.0 to 4.7.0.
  * Use wrap-and-sort -at for debian control files
  * Replaced obsolete pkg-config build dependency with pkgconf.
  * Trim trailing whitespace.
  * Bump debhelper from old 12 to 13.

 -- Petter Reinholdtsen <pere@debian.org>  Mon, 03 Jun 2024 00:32:07 +0200

libtut (0.0.20070706-3) unstable; urgency=medium

  * QA upload.

  [ Niels Thykier ]
  * Set Rules-Requires-Root to no.
  * Rewritten debian/rules to use dh style.
  * Bump Standards-Versions to 4.5.0 - no additional changes required.

  [ Helmut Grohne ]
  * Mark libtut-dev Multi-Arch: foreign. (Closes: #893887)

 -- Niels Thykier <niels@thykier.net>  Fri, 17 Apr 2020 21:02:12 +0000

libtut (0.0.20070706-2) unstable; urgency=medium

  * QA upload.
  * Set maintainer to Debian QA Group. (see #834442)
  * Apply patch from Jari Aalto: (Closes: #668050)
    - Remove deprecated dpatch and upgrade to packaging format "3.0 quilt".
    - Update to Standards-Version to 3.9.3, debhelper to 9, ${misc:Depends}.
    - Add build-arch and build-indep targets; use dh_prep in rules file.

 -- Adrian Bunk <bunk@debian.org>  Tue, 27 Jun 2017 11:16:06 +0300

libtut (0.0.20070706-1) unstable; urgency=low

  * New maintainer. (Closes: #446162)
  * New upstream release (2007-07-06).
  * Removed unzip from Build-Depends and debian/rules
  * Standard-Version 3.7.2--> 3.7.3
  * Added Homepage field to debian/control
  * Patches have been recreated to fit the new upstream version

 -- César Gómez Martín <cesar.gomez@gmail.com>  Tue, 05 Feb 2008 13:44:23 +0100

libtut (0.0.20060329-1) unstable; urgency=low

  * New upstream release.

 -- martin f. krafft <madduck@debian.org>  Fri,  9 Jun 2006 16:24:29 +0200

libtut (0.0.20040326-6) unstable; urgency=low

  * Removed suggestion for libtut-doc, which does not exist.

 -- martin f. krafft <madduck@debian.org>  Sat, 14 May 2005 20:04:33 +0200

libtut (0.0.20040326-5) unstable; urgency=low

  * Fixed missing homepage link in README.docs.

 -- martin f. krafft <madduck@debian.org>  Sat, 14 May 2005 01:17:43 +0200

libtut (0.0.20040326-4) unstable; urgency=low

  * Added documentation (from webpage).

 -- martin f. krafft <madduck@debian.org>  Sat, 14 May 2005 01:05:24 +0200

libtut (0.0.20040326-3) unstable; urgency=low

  * Added README.gcc-3.4 with notes for users of gcc 3.4.
  * pkg-config file now goes to /usr/share/pkgconfig, as supported by newer
    pkg-config libraries.
  * Added versioned suggestion on pkg-config 0.16.0-1.

 -- martin f. krafft <madduck@debian.org>  Fri, 13 May 2005 14:03:39 +0200

libtut (0.0.20040326-2) unstable; urgency=low

  * Fixed Makefiles in examples. (thanks to Petri Latvala for "insisting"
    I fix this, and subsequent testing.)

 -- martin f. krafft <madduck@debian.org>  Wed, 11 May 2005 22:30:00 +0200

libtut (0.0.20040326-1) unstable; urgency=low

  * Initial release (closes: Bug#307774).

 -- martin f. krafft <madduck@debian.org>  Wed, 11 May 2005 21:44:44 +0200
